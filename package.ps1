﻿Param(
	[parameter(Mandatory=$true)]
	[string]
	$Package,

	[parameter(Mandatory=$true)]
	[string]
	$RepoUrl,

	[parameter(Mandatory=$true)]
	[string]
	$Version,

	[string]
	$PrerreleasePostfix
)
ipmo .\Tools\PSake\psake.psm1
Invoke-psake NugetPackage -prop @{ packageName = $Package; repoUrl = $RepoUrl; version = $Version; prerreleasePostfix = $PrerreleasePostfix }
Remove-Module psake