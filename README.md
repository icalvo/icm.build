# Introduction

These are my build scripts, which I use to compile, test, build & publish Nuget package and tag the repository.

I use [psake](https://github.com/psake/psake "psake") for coding the tasks and several small scripts that call Invoke-Psake with appropriate arguments.

Every tool used, except for git and msbuild, is included in the source (NUnit, NuGet.exe, psake), in order to make the process as independent as possible.

The whole building process consists of the following steps:

* Clean dirs
* `git checkout` from repo / just use local repo
* Bump versions
  * `AssemblyInfo.vb` to [asmversion]
  * `[packageid].nuspec` to [nugetversion]
* Compile (msbuild) for each framework
	* There must be a solution file with the form: `[sourcedir]\Src\[packageid].[framework].sln` that will incorporate the lib project and the test project.
    * The lib project files are configured to compile to the output directory `[sourcedir]\Src\[packageid]\Release\[framework]`
    * The test project files are configured to compile to the output directory `[sourcedir]\Src\[packageid].Tests\Release\[framework]`
* Test (NUnit) for each framework
* Pack
  * Copy each framework release dir in `[workingdir]\lib\[framework]`
  * Copy `[packageid].nuspec` to `[workingdir]`
  * Execute NuGet pack
* Nuget Publish
* `git tag [nugetversion]`
* `git push`

