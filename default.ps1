﻿properties { 
	$signKeyPath = "D:\Development\Releases\icm.snk"
	$localNugetPath = $null

	$version = $null
	$prerreleasePostfix = $null
	
	$repoDir = $null
	$repoUrl = $null

	$packageName = $null

	$signAssemblies = $false
	$buildDocumentation = $false
	$buildNuGet = $true
	$zipSource = $false
	$checkoutDir = $null
	$buildDir = resolve-path .
	
	$toolsDir = "$buildDir\Tools"
	$releaseDir = "$buildDir\Release"
	$workingDir = "$buildDir\Working"

	$builds = @(
		@{NunitFramework = "net-3.5"; NugetFramework = "net35"; PsakeFramework = "3.5"; Constants="FrameworkNet35"; Sign=$true};
		@{NunitFramework = "net-4.0"; NugetFramework = "net40"; PsakeFramework = "4.0"; Constants="FrameworkNet40"; Sign=$true};
		@{NunitFramework = "net-4.0"; NugetFramework = "net40-client"; PsakeFramework = "4.0"; Constants="FrameworkNet40"; Sign=$true}
		@{NunitFramework = "net-4.5"; NugetFramework = "net45"; PsakeFramework = "4.5"; Constants="FrameworkNet45"; Sign=$true};
	)
}

task Init {
	if ($repoUrl) {
		$script:checkoutDir = "$buildDir\Checkout"
	}
	else {
		$script:checkoutDir = resolve-path $repoDir
	}
	$script:sourceDir = "$script:checkoutDir\Src"
	$script:docDir = "$script:checkoutDir\Doc"
	$script:zipFileName = $packageName + "." + $version + ".zip"
	$script:mainBuildName = $packageName

	if ($prerreleasePostfix) {
		$script:fullVersion = $version + '-' + $prerreleasePostfix
	}
	else {
		$script:fullVersion = $version
	}
	Write-Host -ForegroundColor Yellow "repoUrl = $repoUrl"
	Write-Host -ForegroundColor Yellow "repoDir = $repoDir"
	Write-Host -ForegroundColor Yellow "buildDir = $buildDir"
	Write-Host
	Write-Host -ForegroundColor Yellow "script:checkoutDir = $script:checkoutDir"
	Write-Host -ForegroundColor Yellow "script:sourceDir = $script:sourceDir"
	Write-Host -ForegroundColor Yellow "script:docDir = $script:docDir"
	Write-Host -ForegroundColor Yellow "script:zipFileName = $script:zipFileName"
	Write-Host -ForegroundColor Yellow "script:mainBuildName = $script:mainBuildName"
	Write-Host -ForegroundColor Yellow "script:fullVersion = $script:fullVersion"
	
}

# Ensure a clean working directory
task Clean -depends Init {
	if ($repoUrl) {
		if (Test-Path -path $script:checkoutDir)
		{
			Write-Output "Deleting Checkout Directory"
			del $script:checkoutDir -Recurse -Force
		}
		Write-Output "Creating Checkout Directory"
		New-Item -Path $script:checkoutDir -ItemType Directory
	}

	if (Test-Path -path $workingDir)
	{
		Write-Output "Deleting Working Directory"
		del $workingDir -Recurse -Force
	}

	Write-Output "Creating Working Directory"
	New-Item -Path $workingDir -ItemType Directory
}

task GetSource -depends Clean {
	if ($repoUrl) {
		Write-Host -ForegroundColor Green "Repo: $repoUrl"
		exec { git clone "$repoUrl" "$script:checkoutDir" } "Error checking out"
	}
	else {
		Write-Host -ForegroundColor Green "No cloning"
	}
	Set-Location "$script:checkoutDir"
}

# Build each solution, optionally signed
task Build -depends GetSource { 

  if ($version) {
		Write-Host -ForegroundColor Green "Updating assembly version to $version"
		Write-Host
		Update-AssemblyInfoFiles $script:sourceDir $version $version
  }

  foreach ($build in $builds)
  {
    $name = $packageName + '.' + $build.NugetFramework
    $sign = ($build.Sign -and $signAssemblies)

	Write-Host -ForegroundColor Green "Building" $name
	Write-Host -ForegroundColor Green "Signed" $sign
	if (-not (Test-Path ".\Src\$name.sln")) {
		Write-Host -ForegroundColor Yellow ".\Src\$name.sln does not exist"
		continue
	}
	
	exec { msbuild "/t:Clean;Rebuild" /v:m /p:Configuration=Release "/p:Platform=Any CPU" /p:AssemblyOriginatorKeyFile=$signKeyPath "/p:SignAssembly=$sign" (GetConstants $build.Constants $sign) ".\Src\$name.sln" } "Error building $name"
  }
}

# Run tests on deployed files
task Test -depends Build {
	foreach ($build in $builds) {
		$name = $packageName + '.' + $build.NugetFramework
		Write-Host -ForegroundColor Green "Testing $name"
		if (-not (Test-Path ".\Src\$name.sln")) {
			Write-Host -ForegroundColor Yellow ".\Src\$name.sln does not exist"
			continue
		}

		$name = $packageName + '.Tests'
		if ($name)
		{
			$framework = $build.NugetFramework
			$nunitfmwork = $build.NunitFramework
			Write-Host -ForegroundColor Green "Copying test assembly $name to deployed directory"
			Write-Host
			robocopy ".\Src\$name\bin\Release\$framework" $workingDir\Deployed\Bin\$framework /NP /XO /XF LinqBridge.dll
			
			Copy-Item -Path ".\Src\$name\bin\Release\$framework\$name.dll" -Destination $workingDir\Deployed\Bin\$framework\

			Write-Host -ForegroundColor Green "Running tests" $name "for framework" $framework
			Write-Host
			exec { & "$toolsDir\NUnit\nunit-console.exe" ".\Src\$name\bin\Release\$framework\$name.dll" /framework:$nunitfmwork /xml:$workingDir\$name.xml } "Error running $name tests"
		}
  }
}

# Optional build documentation, NuGet Pack
task NugetPackage -depends Test -requiredVariables packageName {
	foreach ($build in $builds) {
		#$name = $packageName + '.' + $build.NugetFramework
		#Write-Host -ForegroundColor Green "Packaging $name"
		#if (-not (Test-Path ".\Src\$name.sln")) {
		#	Write-Host -ForegroundColor Yellow ".\Src\$name.sln does not exist"
		#	continue
		#}
		
		#    $finalDir = $build.NugetFramework

		#    robocopy "$script:sourceDir\$name\bin\Release\$finalDir" $workingDir\Package\Bin\$finalDir /NP /XO /XF *.pri
	}

	if ($buildNuGet) {
		New-Item -Path $workingDir\NuGet -ItemType Directory
		Copy-Item -Path "$script:checkoutDir\$packageName.nuspec" -Destination "$workingDir\NuGet\$packageName.nuspec" -recurse

		foreach ($build in $builds) {
			$name = $build.Name
			$framework = $build.NugetFramework

			$name = $packageName + '.' + $build.NugetFramework
			Write-Host -ForegroundColor Green "Packaging $name"
			if (-not (Test-Path ".\Src\$name.sln")) {
				Write-Host -ForegroundColor Yellow ".\Src\$name.sln does not exist"
				continue
			}
		
			New-Item -Path "$workingDir\NuGet\lib\$framework" -ItemType Directory
			Copy-Item "$script:sourceDir\$packageName\bin\Release\$framework\$packageName.*" "$workingDir\NuGet\lib\$framework"
			Get-ChildItem "$script:sourceDir\$packageName\bin\Release\$framework" -Recurse |
				Where-Object { $_.PSIsContainer -eq $true } |
				Foreach-Object { 
					$dest = "$workingDir\NuGet\lib\$framework\" + $_.Name
					New-Item -Path $dest -ItemType Directory
					robocopy $_.FullName $dest /NP /XO /XF }
		}
		if ($version) {
			Write-Host -ForegroundColor Green "Nuget pack version" $script:fullVersion
			exec { & "$toolsDir\NuGet\NuGet.exe" pack "$workingDir\NuGet\$packageName.nuspec" -Version $script:fullVersion -OutputDirectory $workingDir\NuGet }
		}
		else {
			exec { & "$toolsDir\NuGet\NuGet.exe" pack "$workingDir\NuGet\$packageName.nuspec" -Version 0.0.0 -OutputDirectory $workingDir\NuGet }
		}
		move -Path .\*.nupkg -Destination $workingDir\NuGet -Force
		if ($localNugetPath -ne $null) {
			Copy-Item $workingDir\NuGet\*.nupkg $localNugetPath -Force
		}
	}

	if ($buildDocumentation)
	{
	$mainBuild = $builds | where { $_.Name -eq $mainBuildName } | select -first 1
	$mainBuildFinalDir = $mainBuild.Framework
	$documentationSourcePath = "$workingDir\Package\Bin\$mainBuildFinalDir"
	Write-Host -ForegroundColor Green "Building documentation from $documentationSourcePath"

	# Sandcastle has issues when compiling with .NET 4 MSBuild - http://shfb.codeplex.com/Thread/View.aspx?ThreadId=50652
	exec { msbuild "/t:Clean;Rebuild" /p:Configuration=Release "/p:DocumentationSourcePath=$documentationSourcePath" $script:docDir\doc.shfbproj } "Error building documentation. Check that you have Sandcastle, Sandcastle Help File Builder and HTML Help Workshop installed."

	move -Path $workingDir\Documentation\Documentation.chm -Destination $workingDir\Package\Documentation.chm
	move -Path $workingDir\Documentation\LastBuild.log -Destination $workingDir\Documentation.log
	}

	if ($zipSource)
	{
	Copy-Item -Path $script:docDir\readme.txt -Destination $workingDir\Package\
	Copy-Item -Path $script:docDir\versions.txt -Destination $workingDir\Package\Bin\

	robocopy $script:sourceDir $workingDir\Package\Source\Src /MIR /NP /XD .svn bin obj TestResults AppPackages /XF *.suo *.user
	robocopy $buildDir $workingDir\Package\Source\Build /MIR /NP /XD .svn
	robocopy $script:docDir $workingDir\Package\Source\Doc /MIR /NP /XD .svn
	robocopy $toolsDir $workingDir\Package\Source\Tools /MIR /NP /XD .svn

	exec { & "$toolsDir\7-zip\7za.exe" a -tzip $workingDir\$zipFileName $workingDir\Package\* } "Error zipping"
	}
}

# Optional build documentation, add files to final zip
task NugetPush -depends NugetPackage -requiredVariables version {
  gci $workingDir\NuGet\*.nupkg | % -process { exec { & "$toolsDir\NuGet\NuGet.exe" push $_.fullname } }
}


function GetConstants($constants, $includeSigned)
{
  $signed = switch($includeSigned) { $true { ",SIGNED" } default { "" } }

  return "/p:DefineConstants=`"TRACE,$constants$signed`""
}

function GetVersion([string] $majorVersion)
{
    $now = [DateTime]::Now
    
    $year = $now.Year - 2000
    $month = $now.Month
    $totalMonthsSince2000 = ($year * 12) + $month
    $day = $now.Day
    $minor = "{0}{1:00}" -f $totalMonthsSince2000, $day
    
    $hour = $now.Hour
    $minute = $now.Minute
    $revision = "{0:00}{1:00}" -f $hour, $minute
    
    return $majorVersion + "." + $minor
}

function Update-AssemblyInfoFiles ([string] $script:sourceDir, [string] $assemblyVersionNumber, [string] $fileVersionNumber)
{
    $assemblyVersionPattern = 'AssemblyVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
    $fileVersionPattern = 'AssemblyFileVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
    $assemblyVersion = 'AssemblyVersion("' + $assemblyVersionNumber + '")';
    $fileVersion = 'AssemblyFileVersion("' + $fileVersionNumber + '")';
    
    Get-ChildItem -Path $script:sourceDir -r -filter AssemblyInfo.vb | ForEach-Object {
        
        $filename = $_.Directory.ToString() + '\' + $_.Name
        Write-Host $filename
        $filename + ' -> ' + $version
    
        (Get-Content $filename) | ForEach-Object {
            % {$_ -replace $assemblyVersionPattern, $assemblyVersion } |
            % {$_ -replace $fileVersionPattern, $fileVersion }
        } | Set-Content $filename
    }
}