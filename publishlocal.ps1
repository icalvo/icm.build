﻿param (
	[parameter(Mandatory=$true)]
	[string]
	$Package,

	[parameter(Mandatory=$true)]
	[string]
	$RepoDir,

	[parameter(Mandatory=$true)]
	[string]
	$Version,

	[string]
	$PrerreleasePostfix
)
ipmo .\Tools\PSake\psake.psm1
Invoke-psake NugetPush -prop @{ packageName = $Package; repoDir = $RepoDir; version = $Version; prerreleasePostfix = $PrerreleasePostfix }
Remove-Module psake