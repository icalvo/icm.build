﻿param (
	[parameter(Mandatory=$true)]
	[string]
	$Package,

	[parameter(Mandatory=$true)]
	[string]
	$RepoUrl
)
ipmo .\Tools\PSake\psake.psm1
Invoke-psake Test -prop @{ packageName = $Package; repoUrl = $RepoUrl }
Remove-Module psake