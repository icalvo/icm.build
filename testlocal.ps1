﻿param (
	[parameter(Mandatory=$true)]
	[string]
	$Package,

	[parameter(Mandatory=$true)]
	[string]
	$RepoDir
)
ipmo .\Tools\PSake\psake.psm1
Invoke-psake Test -prop @{ packageName = $Package; repoDir = $RepoDir }
Remove-Module psake